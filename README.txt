mass_url.module allows for easy mass URL aliasing, not requiring the use of path.module for many types of common URLs.

Currently this module only supports user page and user blog URLs.

When installed, a user's account page is accessible via http://example.com/user/username, and a user's blog is accessible via http://example.com/blog/username.

As of now, only view functions are available (no edit for account pages), and user names must be entered exactly as they are formatted (including spaces). I hope to implement some kind of fuzzy filtering soon, but that's a big task...
